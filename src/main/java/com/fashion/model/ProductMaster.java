package com.fashion.model;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Table;

import org.hibernate.annotations.OnDelete;
import org.hibernate.annotations.OnDeleteAction;

@Entity
@Table(name = "product_master")
public class ProductMaster {
	@Id
	@Column(name = "product_master_id")
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	private int productMasterId;

	@Column(name = "product_master_name")
	private String productMasterName;

	@ManyToOne(fetch = FetchType.LAZY, optional = false)
	@JoinColumn(name = "brand_id", nullable = false)
	@OnDelete(action = OnDeleteAction.CASCADE)
	private Brands brand;

	@ManyToOne(fetch = FetchType.LAZY, optional = false)
	@JoinColumn(name = "category_id", nullable = false)
	@OnDelete(action = OnDeleteAction.CASCADE)
	private Category category;

	public int getProductMasterId() {
		return productMasterId;
	}

	public void setProductMasterId(int productMasterId) {
		this.productMasterId = productMasterId;
	}

	public String getProductMasterName() {
		return productMasterName;
	}

	public void setProductMasterName(String productMasterName) {
		this.productMasterName = productMasterName;
	}

	public Brands getBrand() {
		return brand;
	}

	public void setBrand(Brands brand) {
		this.brand = brand;
	}

	public Category getCategory() {
		return category;
	}

	public void setCategory(Category category) {
		this.category = category;
	}

}
