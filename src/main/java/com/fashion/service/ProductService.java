package com.fashion.service;

import java.util.ArrayList;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.util.CollectionUtils;

import com.fashion.dao.ProductRepository;
import com.fashion.model.Product;

@Service
public class ProductService {

	@Autowired
	private ProductRepository productRepository;
	
	public void addOrUpdateProduct(Product product) {
		if (product != null) {
			//This will update it
			if (productRepository.existsById(product.getProductId())) {
				productRepository.deleteById(product.getProductId());
			}
			productRepository.save(product);
		}
	}
	
	public List<Product> listProduct() {
		 List<Product>products = new ArrayList<>();  
	     productRepository.findAll().forEach(products::add);  
	     return products;
	}
	
	public List<Product> listProductWithinLimit(int lowerLimit, int upperLimit) {
		 List<Product>products = new ArrayList<>();  
	     productRepository.findAll().forEach(products::add);  
	     
	     if(!CollectionUtils.isEmpty(products)) {
	    	 if(products.size() < upperLimit)
	    		 upperLimit = products.size();
	    	 return products.subList(lowerLimit, upperLimit);
	     }
	     else
	    	 return null;
	}
	
	
	public Product getProductById(int productId) {
		try {
			return productRepository.findById(productId).get();
		}catch(Exception exception) {
			exception.printStackTrace();
		}
		return null;
	}
	
	public void removeProduct(int productId) {
		try {
			Product product = productRepository.findById(productId).get();
			if(null != product){
				product.setDeleted(true);
				addOrUpdateProduct(product);
			}
		}catch(Exception exception) {
			exception.printStackTrace();
		}
		
	}

	public List<Product> getProductsByProductMasterId(int productMasterId) {
		List<Product> products = new ArrayList<Product>();
		List<Product> existingProducts = listProduct();
		for(Product product : existingProducts)
			if(productMasterId == product.getProductMaster().getProductMasterId())
				products.add(product);
		
		return products;
	}

	
	
}
