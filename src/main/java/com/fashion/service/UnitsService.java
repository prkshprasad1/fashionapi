package com.fashion.service;

import java.util.ArrayList;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.util.CollectionUtils;

import com.fashion.dao.UnitRepository;
import com.fashion.model.Units;

@Service
public class UnitsService {

	@Autowired
	private UnitRepository unitRepository;
	
	public void addOrUpdateUnit(Units unit) {
		if (unit != null) {
			//This will update it
			if (unitRepository.existsById(unit.getUnitId())) {
				unitRepository.deleteById(unit.getUnitId());
			}
			unitRepository.save(unit);
		}
	}
	
	public List<Units> listUnits() {
		 List<Units>units = new ArrayList<>();  
	     unitRepository.findAll().forEach(units::add);  
	     return units;
	}
	
	public List<Units> listUnitsWithinLimit(int lowerLimit, int upperLimit) {
		 List<Units>units = new ArrayList<>();  
	     unitRepository.findAll().forEach(units::add);  
	     
	     if(!CollectionUtils.isEmpty(units)) {
	    	 if(units.size() < upperLimit)
	    		 upperLimit = units.size();
	    	 return units.subList(lowerLimit, upperLimit);
	     }
	     else
	    	 return null;
	}
	
	
	public Units getUnitById(int unitId) {
		try {
			return unitRepository.findById(unitId).get();
		}catch(Exception exception) {
			exception.printStackTrace();
		}
		return null;
	}
	
	public void removeUnit(int unitId) {
		try {
			Units unit = unitRepository.findById(unitId).get();
			if(null != unit){
				unit.setDeleted(true);
				addOrUpdateUnit(unit);
			}
		}catch(Exception exception) {
			exception.printStackTrace();
		}
		
	}

	
	
}
