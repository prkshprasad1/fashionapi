package com.fashion.service;

import java.util.ArrayList;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.util.CollectionUtils;

import com.fashion.dao.ManufacturerRepository;
import com.fashion.model.Manufacturer;

@Service
public class ManufacturerService {

	@Autowired
	private ManufacturerRepository manufacturerRepository;
	
	public void addOrUpdateManufacturer(Manufacturer manufacturer) {
		if (manufacturer != null) {
			//This will update it
			if (manufacturerRepository.existsById(manufacturer.getManufacturerId())) {
				manufacturerRepository.deleteById(manufacturer.getManufacturerId());
			}
			manufacturerRepository.save(manufacturer);
		}
	}
	
	public List<Manufacturer> listManufacturer() {
		 List<Manufacturer>manufacturers = new ArrayList<>();  
	     manufacturerRepository.findAll().forEach(manufacturers::add);  
	     return manufacturers;
	}
	
	public List<Manufacturer> listManufacturerWithinLimit(int lowerLimit, int upperLimit) {
		 List<Manufacturer>manufacturers = new ArrayList<>();  
	     manufacturerRepository.findAll().forEach(manufacturers::add);  
	     
	     if(!CollectionUtils.isEmpty(manufacturers)) {
	    	 if(manufacturers.size() < upperLimit)
	    		 upperLimit = manufacturers.size();
	    	 return manufacturers.subList(lowerLimit, upperLimit);
	     }
	     else
	    	 return null;
	}
	
	
	public Manufacturer getManufacturerById(int manufacturerId) {
		try {
			return manufacturerRepository.findById(manufacturerId).get();
		}catch(Exception exception) {
			exception.printStackTrace();
		}
		return null;
	}
	
	public void removeManufacturer(int manufacturerId) {
		try {
			Manufacturer manufacturer = manufacturerRepository.findById(manufacturerId).get();
			if(null != manufacturer){
				manufacturer.setDeleted(true);
				addOrUpdateManufacturer(manufacturer);
			}
		}catch(Exception exception) {
			exception.printStackTrace();
		}
		
	}

	
	
}
