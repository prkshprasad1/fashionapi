package com.fashion.service;

import java.util.ArrayList;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.util.CollectionUtils;

import com.fashion.dao.BrandRepository;
import com.fashion.model.Brands;

@Service
public class BrandsService {

	@Autowired
	private BrandRepository brandRepository;
	
	public void addOrUpdateBrand(Brands brand) {
		if (brand != null) {
			//This will update it
			if (brandRepository.existsById(brand.getBrandId())) {
				brandRepository.deleteById(brand.getBrandId());
			}
			brandRepository.save(brand);
		}
	}
	
	public List<Brands> listBrands() {
		 List<Brands>brands = new ArrayList<>();  
	     brandRepository.findAll().forEach(brands::add);  
	     return brands;
	}
	
	public List<Brands> listBrandsWithinLimit(int lowerLimit, int upperLimit) {
		 List<Brands>brands = new ArrayList<>();  
	     brandRepository.findAll().forEach(brands::add);  
	     
	     if(!CollectionUtils.isEmpty(brands)) {
	    	 if(brands.size() < upperLimit)
	    		 upperLimit = brands.size();
	    	 return brands.subList(lowerLimit, upperLimit);
	     }
	     else
	    	 return null;
	}
	
	
	public Brands getBrandById(int brandId) {
		try {
			return brandRepository.findById(brandId).get();
		}catch(Exception exception) {
			exception.printStackTrace();
		}
		return null;
	}
	
	public void removeBrand(int brandId) {
		try {
			Brands brand = brandRepository.findById(brandId).get();
			if(null != brand){
				brand.setDeleted(true);
				addOrUpdateBrand(brand);
			}
		}catch(Exception exception) {
			exception.printStackTrace();
		}
		
	}

	
	
}
