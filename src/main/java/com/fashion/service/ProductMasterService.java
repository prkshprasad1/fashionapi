package com.fashion.service;

import java.util.ArrayList;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.util.CollectionUtils;

import com.fashion.dao.ProductMasterRepository;
import com.fashion.model.Product;
import com.fashion.model.ProductMaster;

@Service
public class ProductMasterService {

	@Autowired
	private ProductMasterRepository productMasterRepository;
	
	@Autowired
	private ProductService productService;
	
	
	public void addOrUpdateProductMaster(ProductMaster productMaster) {
		if (productMaster != null) {
			//This will update it
			if (productMasterRepository.existsById(productMaster.getProductMasterId())) {
				productMasterRepository.deleteById(productMaster.getProductMasterId());
			}
			productMasterRepository.save(productMaster);
		}
	}
	
	public List<ProductMaster> listProductMaster() {
		 List<ProductMaster>productMasters = new ArrayList<>();  
	     productMasterRepository.findAll().forEach(productMasters::add);  
	     return productMasters;
	}
	
	public List<ProductMaster> listProductMasterWithinLimit(int lowerLimit, int upperLimit) {
		 List<ProductMaster>productMasters = new ArrayList<>();  
	     productMasterRepository.findAll().forEach(productMasters::add);  
	     
	     if(!CollectionUtils.isEmpty(productMasters)) {
	    	 if(productMasters.size() < upperLimit)
	    		 upperLimit = productMasters.size();
	    	 return productMasters.subList(lowerLimit, upperLimit);
	     }
	     else
	    	 return null;
	}
	
	
	public ProductMaster getProductMasterById(int productMasterId) {
		try {
			return productMasterRepository.findById(productMasterId).get();
		}catch(Exception exception) {
			exception.printStackTrace();
		}
		return null;
	}
	
	public void removeProductMaster(int productMasterId) {
		try {
			List<Product> products = productService.getProductsByProductMasterId(productMasterId);
			//If product master does not belongs to any product then only delete it
			if(CollectionUtils.isEmpty(products)) {
				productMasterRepository.deleteById(productMasterId);
			}
		}catch(Exception exception) {
			exception.printStackTrace();
		}
		
	}

	
	
}
