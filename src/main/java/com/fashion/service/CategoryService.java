package com.fashion.service;

import java.util.ArrayList;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.util.CollectionUtils;

import com.fashion.dao.CategoryRepository;
import com.fashion.model.Category;

@Service
public class CategoryService {

	@Autowired
	private CategoryRepository categoryRepository;
	
	public void addOrUpdateCategory(Category category) {
		if (category != null) {
			//This will update it
			if (categoryRepository.existsById(category.getCategoryId())) {
				categoryRepository.deleteById(category.getCategoryId());
			}
			categoryRepository.save(category);
		}
	}
	
	public List<Category> listCategory() {
		 List<Category>categorys = new ArrayList<>();  
	     categoryRepository.findAll().forEach(categorys::add);  
	     return categorys;
	}
	
	public List<Category> listCategoryWithinLimit(int lowerLimit, int upperLimit) {
		 List<Category>categorys = new ArrayList<>();  
	     categoryRepository.findAll().forEach(categorys::add);  
	     
	     if(!CollectionUtils.isEmpty(categorys)) {
	    	 if(categorys.size() < upperLimit)
	    		 upperLimit = categorys.size();
	    	 return categorys.subList(lowerLimit, upperLimit);
	     }
	     else
	    	 return null;
	}
	
	
	public Category getCategoryById(int categoryId) {
		try {
			return categoryRepository.findById(categoryId).get();
		}catch(Exception exception) {
			exception.printStackTrace();
		}
		return null;
	}
	
	public void removeCategory(int categoryId) {
		try {
			Category category = categoryRepository.findById(categoryId).get();
			if(null != category){
				category.setDeleted(true);
				addOrUpdateCategory(category);
			}
		}catch(Exception exception) {
			exception.printStackTrace();
		}
		
	}

	
	
}
