package com.fashion;

public class Test {

	public static void main(String[] args) {
		for (int page = 1; page <= 5; page++) {
			for (int limit = 5; limit <= 20; limit += 5) {
				int lower = ((page - 1) * limit) + 1;
				int upper = (page * limit);

				System.out.println("page " + page + ", " + lower + " to " + upper);
			}
			System.out.println();
		}
	}

}
