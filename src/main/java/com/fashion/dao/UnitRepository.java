package com.fashion.dao;

import org.springframework.data.jpa.repository.JpaRepository;

import com.fashion.model.Units;

public interface UnitRepository extends JpaRepository<Units, Integer> {  
}  