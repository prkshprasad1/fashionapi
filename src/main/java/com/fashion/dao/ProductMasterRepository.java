package com.fashion.dao;

import org.springframework.data.jpa.repository.JpaRepository;

import com.fashion.model.ProductMaster;

public interface ProductMasterRepository extends JpaRepository<ProductMaster, Integer> {  
}  