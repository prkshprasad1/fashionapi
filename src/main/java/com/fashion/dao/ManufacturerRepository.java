package com.fashion.dao;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.repository.CrudRepository;

import com.fashion.model.Manufacturer;

public interface ManufacturerRepository extends JpaRepository<Manufacturer, Integer> {  
}  