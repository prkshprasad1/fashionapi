package com.fashion.dao;

import org.springframework.data.jpa.repository.JpaRepository;

import com.fashion.model.Brands;

public interface BrandRepository extends JpaRepository<Brands, Integer> {  
}  