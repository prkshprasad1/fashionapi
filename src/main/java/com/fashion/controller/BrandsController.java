package com.fashion.controller;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

import com.fashion.model.Brands;
import com.fashion.service.BrandsService;

@CrossOrigin(origins = "*", allowedHeaders = "*")
@RestController
public class BrandsController {

	@Autowired
	private BrandsService brandService;
	
	@RequestMapping(value = "/brands" , method = RequestMethod.GET)
	public List<Brands> listBrands() {
		return brandService.listBrands();
	}
	
	@RequestMapping(value = "/brand/{brandId}" , method = RequestMethod.GET)
	public Brands getBrandById(@PathVariable int brandId) {
		return brandService.getBrandById(brandId);
	}
	
	@RequestMapping(value = "/brand-with-limit/{page}/{limit}" , method = RequestMethod.GET)
	public List<Brands> getBrandWithinLimit(@PathVariable int page, @PathVariable int limit) {
		/*
		 *  page = 1,  1 to 10
			page = 2,  11 to 20
			page = n,  ((n-1)*limit) + 1   to  (n * limit)
		 */
		int lower = ((page -1) * limit) + 1;
		int upper = (page * limit);
		return brandService.listBrandsWithinLimit(lower, upper);
	}
	
	@RequestMapping(value = "/remove-brand/{brandId}" , method = RequestMethod.DELETE)
	public List<Brands> removeBrandById(@PathVariable int brandId) {
		brandService.removeBrand(brandId);
		return brandService.listBrands();
	}
	
	@RequestMapping(value = "/add-brand" , method = RequestMethod.POST)
	public List<Brands> insertBrand(@RequestBody Brands brand) {
		brandService.addOrUpdateBrand(brand);
		return brandService.listBrands();
	}
	
	@RequestMapping(value = "/update-brand" , method = RequestMethod.PUT)
	public List<Brands> updateBrand(@RequestBody Brands brand) {
		brandService.addOrUpdateBrand(brand);
		return brandService.listBrands();
	}
	
}
