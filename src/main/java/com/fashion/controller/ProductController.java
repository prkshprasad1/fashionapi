package com.fashion.controller;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

import com.fashion.model.Product;
import com.fashion.service.ProductService;

@CrossOrigin(origins = "*", allowedHeaders = "*")
@RestController
public class ProductController {

	@Autowired
	private ProductService productService;
	
	@RequestMapping(value = "/products" , method = RequestMethod.GET)
	public List<Product> listProduct() {
		return productService.listProduct();
	}
	
	@RequestMapping(value = "/product/{productId}" , method = RequestMethod.GET)
	public Product getProductById(@PathVariable int productId) {
		return productService.getProductById(productId);
	}
	
	@RequestMapping(value = "/product-with-limit/{page}/{limit}" , method = RequestMethod.GET)
	public List<Product> getProductWithinLimit(@PathVariable int page, @PathVariable int limit) {
		/*
		 *  page = 1,  1 to 10
			page = 2,  11 to 20
			page = n,  ((n-1)*limit) + 1   to  (n * limit)
		 */
		int lower = ((page -1) * limit) + 1;
		int upper = (page * limit);
		return productService.listProductWithinLimit(lower, upper);
	}
	
	@RequestMapping(value = "/remove-product/{productId}" , method = RequestMethod.DELETE)
	public List<Product> removeProductById(@PathVariable int productId) {
		productService.removeProduct(productId);
		return productService.listProduct();
	}
	
	@RequestMapping(value = "/add-product" , method = RequestMethod.POST)
	public List<Product> insertProduct(@RequestBody Product product) {
		productService.addOrUpdateProduct(product);
		return productService.listProduct();
	}
	
	@RequestMapping(value = "/update-product" , method = RequestMethod.PUT)
	public List<Product> updateProduct(@RequestBody Product product) {
		productService.addOrUpdateProduct(product);
		return productService.listProduct();
	}
	
}
