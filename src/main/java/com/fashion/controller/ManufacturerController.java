package com.fashion.controller;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

import com.fashion.model.Manufacturer;
import com.fashion.service.ManufacturerService;

@CrossOrigin(origins = "*", allowedHeaders = "*")
@RestController
public class ManufacturerController {

	@Autowired
	private ManufacturerService manufacturerService;
	
	@RequestMapping(value = "/manufacturers" , method = RequestMethod.GET)
	public List<Manufacturer> listManufacturer() {
		return manufacturerService.listManufacturer();
	}
	
	@RequestMapping(value = "/manufacturer/{manufacturerId}" , method = RequestMethod.GET)
	public Manufacturer getManufacturerById(@PathVariable int manufacturerId) {
		return manufacturerService.getManufacturerById(manufacturerId);
	}
	
	@RequestMapping(value = "/manufacturer-with-limit/{page}/{limit}" , method = RequestMethod.GET)
	public List<Manufacturer> getManufacturerWithinLimit(@PathVariable int page, @PathVariable int limit) {
		/*
		 *  page = 1,  1 to 10
			page = 2,  11 to 20
			page = n,  ((n-1)*limit) + 1   to  (n * limit)
		 */
		int lower = ((page -1) * limit) + 1;
		int upper = (page * limit);
		return manufacturerService.listManufacturerWithinLimit(lower, upper);
	}
	
	@RequestMapping(value = "/remove-manufacturer/{manufacturerId}" , method = RequestMethod.DELETE)
	public List<Manufacturer> removeManufacturerById(@PathVariable int manufacturerId) {
		manufacturerService.removeManufacturer(manufacturerId);
		return manufacturerService.listManufacturer();
	}
	
	@RequestMapping(value = "/add-manufacturer" , method = RequestMethod.POST)
	public List<Manufacturer> insertManufacturer(@RequestBody Manufacturer manufacturer) {
		manufacturerService.addOrUpdateManufacturer(manufacturer);
		return manufacturerService.listManufacturer();
	}
	
	@RequestMapping(value = "/update-manufacturer" , method = RequestMethod.PUT)
	public List<Manufacturer> updateManufacturer(@RequestBody Manufacturer manufacturer) {
		manufacturerService.addOrUpdateManufacturer(manufacturer);
		return manufacturerService.listManufacturer();
	}
	
}
