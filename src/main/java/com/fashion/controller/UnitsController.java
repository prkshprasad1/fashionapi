package com.fashion.controller;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

import com.fashion.model.Units;
import com.fashion.service.UnitsService;

@CrossOrigin(origins = "*", allowedHeaders = "*")
@RestController
public class UnitsController {

	@Autowired
	private UnitsService unitService;
	
	@RequestMapping(value = "/units" , method = RequestMethod.GET)
	public List<Units> listUnits() {
		return unitService.listUnits();
	}
	
	@RequestMapping(value = "/unit/{unitId}" , method = RequestMethod.GET)
	public Units getUnitById(@PathVariable int unitId) {
		return unitService.getUnitById(unitId);
	}
	
	@RequestMapping(value = "/unit-with-limit/{page}/{limit}" , method = RequestMethod.GET)
	public List<Units> getUnitWithinLimit(@PathVariable int page, @PathVariable int limit) {
		/*
		 *  page = 1,  1 to 10
			page = 2,  11 to 20
			page = n,  ((n-1)*limit) + 1   to  (n * limit)
		 */
		int lower = ((page -1) * limit) + 1;
		int upper = (page * limit);
		return unitService.listUnitsWithinLimit(lower, upper);
	}
	
	@RequestMapping(value = "/remove-unit/{unitId}" , method = RequestMethod.DELETE)
	public List<Units> removeUnitById(@PathVariable int unitId) {
		unitService.removeUnit(unitId);
		return unitService.listUnits();
	}
	
	@RequestMapping(value = "/add-unit" , method = RequestMethod.POST)
	public List<Units> insertUnit(@RequestBody Units unit) {
		unitService.addOrUpdateUnit(unit);
		return unitService.listUnits();
	}
	
	@RequestMapping(value = "/update-unit" , method = RequestMethod.PUT)
	public List<Units> updateUnit(@RequestBody Units unit) {
		unitService.addOrUpdateUnit(unit);
		return unitService.listUnits();
	}
	
}
