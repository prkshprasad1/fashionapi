package com.fashion.controller;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

import com.fashion.model.Category;
import com.fashion.service.CategoryService;

@CrossOrigin(origins = "*", allowedHeaders = "*")
@RestController
public class CategoryController {

	@Autowired
	private CategoryService categoryService;
	
	@RequestMapping(value = "/categorys" , method = RequestMethod.GET)
	public List<Category> listCategory() {
		return categoryService.listCategory();
	}
	
	@RequestMapping(value = "/category/{categoryId}" , method = RequestMethod.GET)
	public Category getCategoryById(@PathVariable int categoryId) {
		return categoryService.getCategoryById(categoryId);
	}
	
	@RequestMapping(value = "/category-with-limit/{page}/{limit}" , method = RequestMethod.GET)
	public List<Category> getCategoryWithinLimit(@PathVariable int page, @PathVariable int limit) {
		/*
		 *  page = 1,  1 to 10
			page = 2,  11 to 20
			page = n,  ((n-1)*limit) + 1   to  (n * limit)
		 */
		int lower = ((page -1) * limit) + 1;
		int upper = (page * limit);
		return categoryService.listCategoryWithinLimit(lower, upper);
	}
	
	@RequestMapping(value = "/remove-category/{categoryId}" , method = RequestMethod.DELETE)
	public List<Category> removeCategoryById(@PathVariable int categoryId) {
		categoryService.removeCategory(categoryId);
		return categoryService.listCategory();
	}
	
	@RequestMapping(value = "/add-category" , method = RequestMethod.POST)
	public List<Category> insertCategory(@RequestBody Category category) {
		categoryService.addOrUpdateCategory(category);
		return categoryService.listCategory();
	}
	
	@RequestMapping(value = "/update-category" , method = RequestMethod.PUT)
	public List<Category> updateCategory(@RequestBody Category category) {
		categoryService.addOrUpdateCategory(category);
		return categoryService.listCategory();
	}
	
}
