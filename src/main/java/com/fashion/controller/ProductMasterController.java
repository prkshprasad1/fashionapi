package com.fashion.controller;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

import com.fashion.model.ProductMaster;
import com.fashion.service.ProductMasterService;

@CrossOrigin(origins = "*", allowedHeaders = "*")
@RestController
public class ProductMasterController {

	@Autowired
	private ProductMasterService productMasterService;
	
	@RequestMapping(value = "/productMasters" , method = RequestMethod.GET)
	public List<ProductMaster> listProductMaster() {
		return productMasterService.listProductMaster();
	}
	
	@RequestMapping(value = "/product-master/{productMasterId}" , method = RequestMethod.GET)
	public ProductMaster getProductMasterById(@PathVariable int productMasterId) {
		return productMasterService.getProductMasterById(productMasterId);
	}
	
	@RequestMapping(value = "/product-master-with-limit/{page}/{limit}" , method = RequestMethod.GET)
	public List<ProductMaster> getProductMasterWithinLimit(@PathVariable int page, @PathVariable int limit) {
		/*
		 *  page = 1,  1 to 10
			page = 2,  11 to 20
			page = n,  ((n-1)*limit) + 1   to  (n * limit)
		 */
		int lower = ((page -1) * limit) + 1;
		int upper = (page * limit);
		return productMasterService.listProductMasterWithinLimit(lower, upper);
	}
	
	@RequestMapping(value = "/remove-product-master/{productMasterId}" , method = RequestMethod.DELETE)
	public List<ProductMaster> removeProductMasterById(@PathVariable int productMasterId) {
		productMasterService.removeProductMaster(productMasterId);
		return productMasterService.listProductMaster();
	}
	
	@RequestMapping(value = "/add-product-master" , method = RequestMethod.POST)
	public List<ProductMaster> insertProductMaster(@RequestBody ProductMaster productMaster) {
		productMasterService.addOrUpdateProductMaster(productMaster);
		return productMasterService.listProductMaster();
	}
	
	@RequestMapping(value = "/update-product-master" , method = RequestMethod.PUT)
	public List<ProductMaster> updateProductMaster(@RequestBody ProductMaster productMaster) {
		productMasterService.addOrUpdateProductMaster(productMaster);
		return productMasterService.listProductMaster();
	}
	
}
